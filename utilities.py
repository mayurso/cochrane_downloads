import boto3
import os
import logging
from boto3.session import Session
from ftplib import FTP
import sys
import psycopg2


def get_logger(filename):
    logging.basicConfig(filename='cochrane-download.log', level=logging.DEBUG,
                        format='%(asctime)s \t[%(name)-s] \t%(filename)-s \t%(lineno)-d'
                               ' \t%(levelname)-s: \t%(message)s \r',
                        datefmt='%Y-%m-%dT%T%Z')
    logger = logging.getLogger(filename)

    return logger



def get_s3_client():
    session = Session(
        aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
        aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY')
    )

    return session.client('s3')


def s3_exists(client, bucket, key):
    resp = client.list_objects(
        Bucket=bucket,
        Prefix=key)
    expected_object = resp.get('Contents', [])

    return bool(expected_object)


def get_temp_working_directory():
    return os.getcwd()

def get_ftp_client(source_url):
    ftp_client = FTP(source_url)
    return ftp_client

def dbConnet():
    try:
        conn = psycopg2.connect(host='18.217.130.132', user='postgres', password='pos123', dbname='medmeme')
        return conn
    except psycopg2.Error as e:
        return e

def watch_fs():
    pass