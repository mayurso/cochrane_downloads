from splinter.browser import Browser
import yaml
import heapq
import time
from selenium import webdriver
import os
from utilities import (
    get_s3_client,
    s3_exists,
    get_logger)
import datetime
import glob
import zipfile

class SplinterScraper:

    browser = Browser('chrome') #, headless=True)
    q = []
    nextpage = None
    recursive_flag = False
    recursive_q = []
    # cfg = None
    zipf = None

    def __init__(self):
        self.logger = get_logger(__name__)
        self.current_dir = os.getcwd()
        self.sourcename = ''

        self.download_dir = 'C:/Users/Mayur Sonawane/Downloads'
        # os.path.join(
        #     '$HOME',
        #     'Downloads'
        # )


    def loadConfig(self):
        files = os.listdir(os.path.join(self.current_dir, 'config_yaml'))
        for file in files:
            try:
                with open(os.path.join(self.current_dir, 'config_yaml',file), 'r') as configfile:
                    self.cfg = yaml.load(configfile)
                for section in self.cfg:
                    heapq.heappush(self.q, (self.cfg[section]['sequence'], self.cfg[section]))
            except Exception as e:
                self.logger.info(e)

    def methodCall(self):

        while self.q:
            func_name = self.q[0][1]['action']
            call_at = self.q[0][1]['sequence']
            print(call_at)
            func = getattr(splinter, func_name)
            if 'formFill' in func_name:
                params = (self.q[0][1]['element'], self.q[0][1]['value'])
                func(*params)
            elif 'userClick' in func_name:
                params = self.q[0][1]['element']
                func(params)
            elif 'browserWait' in func_name:
                params = self.q[0][1]['value']
                func(params)
            elif 'visit' in func_name:
                params = self.q[0][1]['value']
                func(params)
            elif 'saveContent' in func_name:
                pass

            elif 'checkIfNextPage' in func_name:
                params = self.q[0][1]['value']
                func(params)
            elif 'clickIfNextPage' in func_name:
                params = self.nextpage    #self.q[0][1]['value']
                if params:
                    func(params)



            if not self.recursive_flag:
                heapq.heappop(self.q)
            else:
                popitem = heapq.heappop(self.q)
                heapq.heappush(self.recursive_q, popitem)

                if len(self.q) == 0:
                    self.q = self.recursive_q
                    self.recursive_q = []


    def checkIfNextPage(self, nextPage):
        try:
            time.sleep(3)
            self.nextpage = self.browser.find_by_xpath(nextPage).first
            if self.nextpage:
                self.recursive_flag = True
            else: self.recursive_flag = False
            #return True
        except:
            print("No Next Page")
            self.nextpage = None
            self.recursive_flag = False

    def clickIfNextPage(self, nextPage):
        try:
            #nextpage = self.browser.find_by_xpath(nextPage)[0]
            nextPage.click()

            try:
                self.browser.wait_time(5)
            except Exception as e:
                time.sleep(3)
                print(e)

        except:
            print("Error while clicking on Next Page")


    def checkNextPage(self):
        try:
            nextpage = self.browser.find_by_xpath(
                '//fieldset[@class="paginationFilter"]/ol/li[@class="selected"]/following-sibling::li')[0]

            return nextpage
        except:
            print("Error while find the siblings")
            return False

    def visit(self, url):
        self.browser.visit(url)

    def userClick(self, element):
        #method to be called when a item needs to be clicked
        try:
            item = self.browser.find_by_css(element).first
            item.click()
        except Exception as e:
            item = self.browser.find_by_xpath(element).first
            item.click()

    def formFill(self, element, value):
        self.browser.fill(element, value)

    def browserWait(self, time):
        self.browser.wait_time(time)

    def wait(self):
        self.browser.wait_time()
    def navigator(self, partial_href):
        self.browser.find_link_by_partial_href(partial_href).click()

    def saveContent(self):
        upload_to_s3 = False
        s3_client = get_s3_client()

        bucket_name = 'mmcapturepoctemp'
        s3_root = os.path.join(
            'data',
            datetime.datetime.now().strftime('%Y/%m/%d'),
            'structured',
            'raw'
        )
        filename = max([f for f in os.listdir(self.download_dir)], key=os.path.getctime)
        s3_file_key = os.path.join(s3_root, filename)

        if not s3_exists(s3_client, bucket_name, s3_file_key):
            upload_to_s3 = True

        if upload_to_s3:
            s3_client.upload_file(
                filename,
                bucket_name,
                s3_file_key)
            # os.remove(filename)

    def windowClose(self):
        window = self.browser.windows[0]
        #options = webdriver.ChromeOptions()
        #options.add_argument('download.default_directory=%s' % (os.path.join(os.path.expanduser('~'), self.sourcename)))
        try:
            window.close()
        except:
            print('close')

    def zipfiles(self, zip_name, file_name):
        os.chdir(self.download_dir)  #'/Downloads'  current directory

        for file in glob.glob('{}*'.format(zip_name)):       #citation-export
            try:
                mtime = os.path.getmtime(file)
            except OSError:
                mtime = 0
            date = time.strptime(time.ctime(mtime))
            last_modified_date = '{}_{}_{}'.format(date.tm_year, date.tm_mon, date.tm_mday)
            print(file)

            if not self.zipf:
                self.zipf = zipfile.ZipFile('{}_{}.zip'.format(file_name,last_modified_date), 'w', zipfile.ZIP_DEFLATED)
            self.zipf.write(file)  #os.path.join(dest,file))
        self.zipf.close()






splinter = SplinterScraper()
splinter.loadConfig()
splinter.methodCall()
splinter.zipfiles('citation-export', 'cochrane')
splinter.saveContent()
splinter.windowClose()